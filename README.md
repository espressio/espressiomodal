# Espressio Modal version 1.2

Espressio jQuery-based modal windows plugins.

## EspressioModalDefaults

EspressioModalDefaults object allow you override default options in your projects.
Just put something like this somewhere after esp-modal.js load:

	EspressioModalDefaults.removeOnHide = true;

Available default values:

	var EspressioModalDefaults = {
    
    	// wrapper element additional class
    	wrapperClass: null,
    
    	// all elements classes prefix
    	classPrefix: 'esp-modal-',
    
    	// use vertical aligner element
    	withAligner: true,
    
    	// element to place modal in
    	// set NULL for body
    	parentElem: null,
    
    	// dialogs buttons default class
    	buttonsDefaultClass: 'btn',
    
    	// default apply btn additional class
    	btnApplyClass: null,
    
    	// default cancel btn additional class
    	btnCancelClass: null,
    
    	// remove modal elements on dialog hide
    	removeOnHide: false,
    
    	// close icon in title
    	// set TRUE for default
    	// set jQuery element for custom button
    	closeIcon: false
    };

## EspressioModal

EspressioModal is the main plugin class. All others EspressioModal classes use it.
You can use it to show custom content in modal window.

Example:
 
	var modal = new EspressioModal(options, removeOnHideOptionValue);
	modal.setContent('This is my great modal content');
	modal.setTitle('Hello');
	modal.setButton(
		{
			text: 'Close'
		}
	);
 
### EspressioModal options

* id: Object identifier. Used in container id attribute. Set empty to generate value.
* wrapperClass: class, which will be added to wrapper element.
* classPrefix: prefix to add to elements classes, 'esp-modal-' by default.
* withAligner: add vertical aligner element, true by default.
* parentElem: element to place modal window in. Set null for body.
* buttonsDefaultClass: default buttons class. 'btn' by default.
* btnApplyClass: default apply buttons class.
* btnCancelClass: default cancel buttons class.
* removeOnHide: remove modal elements on modal hide.
* withCloseIcon: add close icon element to modal window header. false by default.

### EspressioModal methods

#### setOptions(optionsObject)
Extend current instance options with optionsObject

#### getOptions()
Get options object

#### option(key, defaultValue)
Get option value by key. If option does not exist, defaultValue will be returned.

#### show()
Show modal window

#### hide()
Hide modal window

#### setContent(data)
Set data (string or jQuery instance) as modal window content

#### setText(text)
Set modal content as plain text 

#### setTitle(title)
Set modal window title

#### setButtons(buttons)
Set buttons by array

#### setButton(btn)
Set button by options object or jQuery instance. 
Button options:

* tag: button element tag name, 'a' by default.
* tagIsSingle: element has no closing tag, false by default.
* text: button text.
* attribs: object of element attributes.
* onclick: function or array of functions to call on click.
* disableOnclickEvent: disable on click handlers.
* closeOnClick: hide modal on button click.

#### reset()
Reset instance data and elements.

#### remove()
Remove modal elements.

#### elem(elem)
If elem param is empty, object with modal elements will be returned.
Otherwise, specified element jQuery instance will be returned.

#### isOpen()
Check if modal window has active state.

### EspressioModal events
When some event is happening with EspressioModal instance, plugin calls the trigger on the wrapper element.
You can use it by jQuery.on() function. For example:

	modal.elem().wrapper.on('espModalHide', function() {
		// Do some actions
	});
	
Available events:

#### espModalShow
Triggers each time the EspressioModal.show() is called.

#### espModalHide
Triggers each time the EspressioModal.hide() is called.

#### espModalRemove
Triggers each time the EspressioModal.remove() is called.

#### espModalReset
Triggers each time the EspressioModal.reset() is called.

## EspressioModalForm

Plugin to show custom form in modal window.

Usage example: 

	var form = $('#some-form');
	var modalForm = new EspressioModalForm(form, 'Edit object');
	
	modalForm.form().submit(function() {
		// do some actions
	});
	
Constructor parameters:

* form: jQuery selector string or jQuery element object
* title: modal window title (see EspressioModal.setTitle())
* buttons: buttons array (see EspressioModal.setButtons()). If empty, save and cancel buttons will be added
* espModalOptions: options for EspressioModal object (see EspressioModal.setOptions())

### EspressioModalForm methods

#### form()

Get form element jQuery object.

## EspressioModalConfirm

Plugin to show confirmation window.

Usage example:

	var confirm = new EspressioModalConfirm(
		'Are you sure you want to delete this item?',
		'Delete',
		{
			'askValue': false
		}
	);
	
	confirm.form().on('confirm', function() {
		// Delete item
	});
	
Usage with askValue enabled:

	var confirm = new EspressioModalConfirm(
        'Please, enter your first name',
        'User info',
        {
            'inputName': 'firstName'
        }
    );
    
    confirm.form().on('confirm', function(e, d) {
        var userName = d.formData.firstName;
        // Do some actions
    });
    
Constructor parameters:

* text: confirmation text
* title: modal window title (see EspressioModal.setTitle())
* options: EspressioModalConfirm options object
* espModalOptions: options for EspressioModal object (see EspressioModal.setOptions())
    
    
### EspressioModalConfirm events
EspressioModalConfirm triggers two events on form element (see EspressioModalConfirm.form()):

#### confirm
Triggers when OK button is clicked or form submitted by Enter.

#### confirmCancel
Triggers when modal window is closed without confirmation.
    
### EspressioModalConfirm options

* input: jQuery input object to ask value. Set empty to render default text input.
* askValue: set false to show only OK/Cancel buttons without input.
* defaultValue: if input enabled, default value to render in it (if default input used).
* inputName: input name attribute (if default input used).
* confirmButton: 'OK' button parameters (see EspressioModal.setButton()).
* cancelButton: 'Cancel' button parameters (see EspressioModal.setButton()).

### EspressioModalConfirm methods

#### modalForm()
Get created by object EspressioModalForm instance.

#### form()
Get form element jQuery object.

## EspressioModalAlert

Plugin to show modal alert window.

Usage example:

	var alert = new EspressioModalAlert(
		'Item was successfully deleted',
		'Delete'
	);
	
Constructor parameters:

* text: modal window text.
* title: modal window title (see EspressioModal.setTitle())
* button: 'OK' button parameters (see EspressioModal.setButton()).
* espModalOptions: options for EspressioModal object (see EspressioModal.setOptions())

Plugin has no methods to call.