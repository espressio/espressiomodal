"use strict";

/*!
 * Espressio Modal
 * jQuery-based modal windows plugins
 * @copyright Copyright (c) 2012-2016 Espressio Labs Ltd. (https://bitbucket.org/espressio)
 * @author    Sergey Basov (sergey@espressio.ru)
 */

/**
 * Espressio Modal default options
 * @type {{}}
 */
var EspressioModalDefaults = {

	// wrapper element additional class
	wrapperClass: null,

	// all elements classes prefix
	classPrefix: 'esp-modal-',

	// use vertical aligner element
	withAligner: true,

	// element to place modal in
	// set NULL for body
	parentElem: null,

	// dialogs buttons default class
	buttonsDefaultClass: 'btn',

	// default apply btn additional class
	btnApplyClass: null,

	// default cancel btn additional class
	btnCancelClass: null,

	// remove modal elements on dialog hide
	removeOnHide: false,

	// close icon in title
	// set TRUE for default
	// set jQuery element for custom button
	closeIcon: false
};

/**
 * Espressio CMS
 * Modal dialog
 *
 * @copyright Copyright (c) 2012-2016 Espressio Labs Ltd. (http://espressio-labs.ru)
 * @author    Sergey Basov (sergey@espressio.ru)
 */
var EspressioModal = function (setOptionsObject, removeOnHideOptionValue) {

	/**
	 * Instance options
	 * @type {Object}
	 * @see  {EspressioModalDefaults}
	 */
	var options = {
		id: null,
		wrapperClass: null,
		classPrefix: 'esp-modal-',
		withAligner: true,
		parentElem: null,
		buttonsDefaultClass: 'btn',
		btnApplyClass: null,
		btnCancelClass: null,
		removeOnHide: false,
		withCloseIcon: false
	};

	/**
	 * Object instance
	 * @type {EspressioModal}
	 */
	var instance = this;

	/**
	 * Mode constants
	 * @type {Object}
	 */
	var MODE = {
		HTML: 'html',
		CSS: 'css'
	};

	/**
	 * Instance identificator
	 * @type {String}
	 */
	var instanceId = null;

	/**
	 * Elements jQuery objects
	 * @type {Object}
	 */
	var elems = {};

	/**
	 * Parent element object
	 * @type jQuery
	 */
	var parentElem = null;

	/**
	 * Object initialization
	 * @return EspressioModal
	 */
	this.init = function () {

		this.setOptions(EspressioModalDefaults);

		if (setOptionsObject) {
			if (typeof removeOnHideOptionValue != 'undefined') {
				setOptionsObject.removeOnHide = (!!removeOnHideOptionValue);
			}
			this.setOptions(setOptionsObject);
		}
		window.console && console.log(
			'EspressioModal is ready.'
		);

		return this;
	};

	/**
	 * Set options
	 * @param {{}} optionsObject
	 * @return EspressioModal
	 */
	this.setOptions = function (optionsObject) {

		if (!optionsObject) {
			return this;
		}

		options = $.extend(
			{},
			options,
			optionsObject
		);

		return this;
	};

	/**
	 * Get options
	 * @returns {Object}
	 */
	this.getOptions = function () {
		return options;
	};

	/**
	 * Get option value
	 * @param {string} key
	 * @param [defaultValue]
	 * @returns {*}
	 */
	this.option = function (key, defaultValue) {
		if (typeof defaultValue == 'undefined') {
			defaultValue = null;
		}
		if (typeof options[key] == 'undefined') {
			return defaultValue;
		}

		return options[key];
	};

	/**
	 * Show
	 * @return EspressioModal
	 */
	this.show = function () {

		if (!this.elem().buttons || !this.elem().buttons.length) {
			this.elem().buttonsBox.addClass('empty');
		} else {
			this.elem().buttonsBox.removeClass('empty');
		}

		this.elem().wrapper.addClass('active');
		this._parent().addClass('espressio-modal-active');

		this.elem().wrapper.trigger('espModalShow');

		return this;
	};

	/**
	 * Hide
	 * @return EspressioModal
	 */
	this.hide = function () {

		this.elem().wrapper.removeClass('active');
		this._parent().removeClass('espressio-modal-active');

		this.elem().wrapper.trigger('espModalHide');

		if (options.removeOnHide) {
			this.remove();
		}

		return this;
	};

	/**
	 * Hide
	 * @return boolean
	 */
	this.hideCallback = function (e) {

		if (e) {
			e.preventDefault && e.preventDefault();
		}

		instance.hide();

		return false;
	};

	/**
	 * Set content
	 * @param {jQuery|string} data
	 * @return EspressioModal
	 */
	this.setContent = function (data) {

		if (data instanceof jQuery) {
			data.prependTo(this.elem().content);
		} else {
			this.elem().content.html(data);
		}

		return this;
	};

	/**
	 * Set title
	 * @param {jQuery|String} title
	 * @return EspressioModal
	 */
	this.setTitle = function (title) {

		if (title instanceof jQuery) {
			title.prependTo(this.elem().title);
		} else {
			this.elem().title.html(title);
		}

		if (this.elem().title.html() == '') {
			this.elem().title.addClass('empty');
		} else {
			this.elem().title.removeClass('empty');
		}

		if (options.closeIcon) {
			if (!(options.closeIcon instanceof jQuery)) {
				options.closeIcon = $('<a href="#" ' + this._class('close-icon', MODE.HTML) + '></a>');
			}
			var closeIcon = options.closeIcon.clone();
			closeIcon.click(instance.hideCallback);
			closeIcon.appendTo(this.elem().title);
			this.elem().title.addClass('with-close-icon');
		}

		return this;
	};

	/**
	 * Set buttons
	 * @param {Array|jQuery|String} buttons
	 * @return EspressioModal
	 */
	this.setButtons = function (buttons) {

		if (!$.isArray(buttons)) {
			buttons = [buttons];
		}

		$.each(buttons, function(i, btn) {
			if (btn instanceof jQuery) {
				if (options.buttonsDefaultClass) {
					btn.addClass(options.buttonsDefaultClass);
				}
				btn.appendTo(instance.elem().buttonsBox);
				instance.elem().buttons = instance.elem().buttonsBox.children();
			} else {
				instance.setButton(btn);
			}
		});

		return this;
	};

	/**
	 * Set one button
	 * @param {Object} btn
	 * @return EspressioModal
	 */
	this.setButton = function (btn) {

		if (!btn) {
			btn = {};
		}

		if (btn instanceof jQuery) {

			var obj = btn;

		} else {

			if (typeof btn == 'string') {
				btn = {
					text: btn
				};
			}

			btn = $.extend(
				true,
				{
					tag: 'a',
					tagIsSingle: false,
					text: 'OK',
					attribs: {
						href: '#'
					},
					onclick: instance.hideCallback,
					disableOnclickEvent: false,
					closeOnClick: false
				},
				btn
			);

			if (!btn.attribs['class'] && options.btnApplyClass) {
				btn.attribs['class'] = options.btnApplyClass;
			}

			var html = '<' + btn.tag + ' ';
			var attribs = [];

			$.each(btn.attribs, function(key, attr) {
				attribs.push(
					key + '="' + (attr !== true ? attr : key) + '"'
				);
			});

			html += attribs.join(' ');

			if (!btn.tagIsSingle) {
				html += '>' + btn.text + '</' + btn.tag + '>'
			} else {
				html += ' />';
			}

			var obj = $(html);

			if (btn.onclick && !btn.disableOnclickEvent) {
				if (!$.isArray(btn.onclick)) {
					btn.onclick = [btn.onclick];
				}

				$.each(btn.onclick, function(i, handler) {
					obj.click(handler);
				});
			}

			if (btn.closeOnClick) {
				obj.click(instance.hideCallback);
			}
		}

		if (options.buttonsDefaultClass) {
			obj.addClass(options.buttonsDefaultClass);
		}

		obj.appendTo(this.elem().buttonsBox);
		this.elem().buttons = this.elem().buttonsBox.children();

		return this;
	};

	/**
	 * Alias for old versions
	 * @param {jQuery|String} text
	 */
	this.setText = function (text) {
		return this.setContent(text);
	};

	/**
	 * Reset all
	 * @return {this}
	 */
	this.reset = function () {

		this.elem().wrapper.trigger('espModalReset');

		this.elem().title.html('');
		this.elem().title.addClass('empty');
		this.elem().content.html('');
		this.elem().buttonsBox.html('');
		this.elem().buttons = null;

		window.console && console.log(
			'Reset EspressioModal #' + this._id()
		);

		return this;
	};

	/**
	 * Destruct elements
	 * @return EspressioModal
	 */
	this.remove = function () {

		if (!elems || !elems.wrapper) {
			return this;
		}

		window.console && console.log(
			'Remove EspressioModal #' + this._id()
		);

		this.elem().wrapper.removeClass('active');
		this._parent().removeClass('espressio-modal-active');

		this.elem().wrapper.trigger('espModalRemove');

		this.elem().wrapper.remove();

		this.elems = {};

		return this;
	};

	/**
	 * Get element
	 * @param  {String} [elem] (Optional)
	 * @return {jQuery|Object}
	 */
	this.elem = function (elem) {

		if (!elems.wrapper) {

			var wrapper = $(this._id(MODE.CSS));

			if (!wrapper.is(this._id(MODE.CSS))) {

				var html = '<div '
					+ this._class('wrapper' + (options.wrapperClass ? ' ' + options.wrapperClass : ''), MODE.HTML) + ' '
					+ this._id(MODE.HTML)
					+ '>'
					+ '<div ' + this._class('overlay', MODE.HTML) + '></div>'
					+ '<div ' + this._class('box', MODE.HTML) + '>'
					+ '<div ' + this._class('title empty', MODE.HTML) + '></div>'
					+ '<div ' + this._class('content', MODE.HTML) + '></div>'
					+ '<div ' + this._class('buttons', MODE.HTML) + '></div>'
					+ '</div>'
					+ (options.withAligner ? '<div ' + this._class('aligner', MODE.HTML) + '></div>' : '')
					+ '</div>';

				wrapper = $(html);

				var prevWrapper = this._parent().children(this._class('wrapper'));
				if (prevWrapper.is(this._class('wrapper'))) {
					wrapper.insertAfter(prevWrapper);
				} else {
					wrapper.prependTo(this._parent());
				}

				this._setCommonEvents();

				window.console && console.log(
					'Created elements for EspressioModal ' + this._id(MODE.CSS)
				);
			}

			elems.wrapper = wrapper;
			elems.overlay = wrapper.children(this._class('overlay'));
			elems.box = wrapper.children(this._class('box'));
			elems.title = elems.box.children(this._class('title'));
			elems.content = elems.box.children(this._class('content'));
			elems.buttonsBox = elems.box.children(this._class('buttons'));
			elems.buttons = null;
		}

		if (!elem) {
			return elems;
		}

		return elems[elem];
	};

	/**
	 * Check if modal window is active
	 * @return {Boolean}
	 */
	this.isOpen = function () {
		return this.elem().wrapper.hasClass('active');
	};

	/**
	 * Get instance ID
	 * @return {String}
	 */
	this._id = function (mode) {

		if (instanceId === null) {
			if (options.id) {
				instanceId = options.id;
			} else {
				instanceId = new Date().getTime() + '' + Math.floor(Math.random() * 10E9);
			}
		}

		if (!mode) {
			return instanceId;
		}

		return (
			mode == MODE.HTML ?
			'id="' + options.classPrefix + instanceId + '"'
				: '#' + options.classPrefix + instanceId
		);
	};

	/**
	 * Get class name with prefix
	 * @param  {String} className
	 * @param  {String} [mode]      FALSE|css|html
	 * @return {String}
	 */
	this._class = function (className, mode) {

		if (!className) {
			return '';
		}

		if (typeof mode == 'undefined') {
			mode = MODE.CSS;
		}

		className = options.classPrefix + className;

		if (!mode) {
			return className;
		}

		return (mode == MODE.HTML ? 'class="' + className + '"' : '.' + className);
	};

	/**
	 * Get parent element object
	 * @return {jQuery}
	 */
	this._parent = function () {

		if (!parentElem) {
			if (!options.parentElem) {
				parentElem = $('body');
			} else if (typeof options.parentElem == 'string') {
				parentElem = $(options.parentElem).first();
				if (!parentElem.is(options.parentElem)) {
					throw new Error(
						'Element given as parent to EspressioModal does not exist'
					);
				}
			} else if (options.parentElem instanceof jQuery) {
				parentElem = options.parentElem;
			} else {
				throw new Error(
					'Parent element must be a jQuery object or an identifier string, ' + (typeof options.parentElem) + ' given'
				);
			}

			window.console && console.log(
				'Set parent element for EspressioModal (' + this._id(MODE.CSS) + '): ',
				parentElem
			);
		}

		return parentElem;
	};

	/**
	 * Set common elements events
	 * @return EspressioModal
	 */
	this._setCommonEvents = function () {

		$(function () {

			instance.elem().overlay.click(instance.hideCallback);

			$('body').keyup(function (e) {
				if (e.keyCode && e.keyCode == 27 && instance.isOpen()) {
					instance.hide();
				}
			});

		});

		return this;
	};

	this.init();

	return this;
};

/**
 * Espressio CMS
 * Modal plugin prototype
 *
 * @copyright Copyright (c) 2012-2014 Espressio Labs Ltd. (http://espressio-labs.ru)
 * @author      Sergey Basov (sergey@espressio.ru)
 */
var EspressioModalPlugin = function () {

	/**
	 * EspressioModal options
	 * @type {Object}
	 */
	this._modalOptions = {};

	/**
	 * EspressioModal instance
	 * @type {EspressioModal}
	 */
	this._modal = null;

	/**
	 * Get EspressioModal instance
	 * @return {EspressioModal}
	 */
	this.modal = function () {

		if (!this._modal) {
			this._modal = new EspressioModal(this._modalOptions);
		}

		return this._modal;
	};

	/**
	 * Set options for EspressioModal instance
	 * @param {Object} options
	 */
	this.setModalOptions = function (options) {

		if (!this._modal) {
			this._modalOptions = options;
		} else {
			this.modal().setOptions(options);
		}

		return this;
	};
};

/**
 * Espressio CMS
 * Modal form
 *
 * @copyright Copyright (c) 2012-2014 Espressio Labs Ltd. (http://espressio-labs.ru)
 * @author      Sergey Basov (sergey@espressio.ru)
 */
var EspressioModalForm = function (form, title, buttons, espModalOptions) {

	/**
	 * Get form object
	 * @return {jQuery}
	 */
	this.form = function () {

		if (!form) {
			throw new Error(
				'Empty form param in EspressioModalForm'
			);
		}

		if (typeof form == 'string') {
			form = $(form);
		} else if (!(form instanceof jQuery)) {
			throw new Error(
				'Form param in EspressioModalForm must be a jQuery object or HTML string, '
				+ (typeof form) + ' given'
			);
		}

		return form;
	};

	/**
	 * Initialize and show
	 * @return EspressioModalForm
	 */
	this.init = function () {

		var form = this.form();

		this.setModalOptions(espModalOptions);

		if (!buttons) {
			var submit = function (e) {
				e && e.preventDefault && e.preventDefault();
				form.submit();
				return false;
			};
			var save = {
				text: 'Save',
				attribs: {},
				onclick: submit
			};
			var cancel = {
				text: 'Cancel',
				attribs: {}
			};

			if (this.modal().option('btnApplyClass')) {
				save.attribs['class'] = this.modal().option('btnApplyClass');
			}
			if (this.modal().option('btnCancelClass')) {
				cancel.attribs['class'] = this.modal().option('btnCancelClass');
			}

			buttons = [save, cancel];
		}

		this.modal().reset();
		this.modal().setContent(form);
		this.modal().setButtons(buttons);

		if (title && typeof title == 'string') {
			this.modal().setTitle(title);
		}

		this.modal().elem().wrapper.on('espModalShow', function () {
			form.find('input, textarea, select').first().focus().select();
		});

		this.modal().show();

		return this;
	};

	this.init();

	return this;
};

/**
 * Espressio CMS
 * Modal confirm
 *
 * @copyright Copyright (c) 2012-2014 Espressio Labs Ltd. (http://espressio-labs.ru)
 * @author      Sergey Basov (sergey@espressio.ru)
 */
var EspressioModalConfirm = function (text, title, options, espModalOptions) {

	/**
	 * Esp modal form object
	 * @type {EspressioModalForm}
	 */
	this._modalForm = null;

	/**
	 * Get modal form instance
	 * @return {EspressioModalForm}
	 */
	this.modalForm = function () {
		return this._modalForm;
	};

	/**
	 * Get form object
	 * @return {jQuery}
	 */
	this.form = function () {
		return this.modalForm().form();
	};

	/**
	 * Initialize and show
	 * @return EspressioModalConfirm
	 */
	this.init = function () {

		options = $.extend(
			true,
			{
				input: null,
				askValue: true,
				defaultValue: '',
				inputName: 'data',
				confirmButton: {
					text: 'OK'
				},
				cancelButton: {
					text: 'Cancel'
				}
			},
			options
		);

		espModalOptions = $.extend(
			{},
			espModalOptions,
			{
				removeOnHide: true
			}
		);
		this.setModalOptions(espModalOptions);

		if (!options.input || !(options.input instanceof jQuery)) {
			options.input = $(
				'<input '
				+ 'type="' + (options.askValue ? 'text' : 'hidden') + '" '
				+ 'name="' + (options.inputName ? options.inputName : 'data') + '" '
				+ 'value="' + (options.defaultValue ? options.defaultValue : '') + '" '
				+ '/>'
			);
		}

		var form = $(
			'<form method="POST" class="espressio-modal-confirm-form">'
			+ '<label for="data">' + (text ? text : 'Value:') + '</label>'
			+ '</form>'
		);

		form.submit(function (e) {
			e.preventDefault && e.preventDefault();
			return false;
		});

		var submit = function (e) {
			e && e.preventDefault && e.preventDefault();

			var fdata = {};
			var data = form.serializeArray();

			$.each(data, function (index, data) {
				fdata[data.name] = form.find('[name="' + data.name + '"]').val();
			});

			form.trigger('confirm', {formData: fdata});
			return false;
		};

		var cancel = function (e) {
			e.preventDefault && e.preventDefault();
			form.trigger('confirmCancel');
			return false;
		};

		var confirmBtn = {
			text: 'OK',
			attribs: {}
		};
		var cancelBtn = {
			text: 'Cancel',
			attribs: {}
		};

		if (this.modal().option('btnApplyClass')) {
			confirmBtn.attribs['class'] = this.modal().option('btnApplyClass');
		}
		if (this.modal().option('btnCancelClass')) {
			cancelBtn.attribs['class'] = this.modal().option('btnCancelClass');
		}

		confirmBtn = $.extend(
			confirmBtn,
			options.confirmButton,
			{
				onclick: submit,
				closeOnClick: true
			}
		);

		cancelBtn = $.extend(
			cancelBtn,
			options.cancelButton,
			{
				onclick: cancel,
				closeOnClick: true
			}
		);

		var buttons = [confirmBtn, cancelBtn];

		form.append(options.input);

		this._modalForm = new EspressioModalForm(form, title, buttons, espModalOptions);

		// Submit on Enter

		var instance = this;

		options.input.keyup(function (e) {
			if (e.keyCode == 13) {
				submit();
				instance._modalForm.modal().hide();
			}
		});

		return this;
	};

	this.init();

	return this;
};

/**
 * Espressio CMS
 * Modal alert
 *
 * @copyright Copyright (c) 2012-2014 Espressio Labs Ltd. (http://espressio-labs.ru)
 * @author      Sergey Basov (sergey@espressio.ru)
 */
var EspressioModalAlert = function (text, title, button, espModalOptions) {

	/**
	 * Initialize and show
	 * @return EspressioModalAlert
	 */
	this.init = function () {

		espModalOptions = $.extend(
			{},
			espModalOptions,
			{
				removeOnHide: true
			}
		);

		this.setModalOptions(espModalOptions);

		this.modal().reset();
		this.modal().setContent(text);
		this.modal().setButton(button);

		if (title && typeof title == 'string') {
			this.modal().setTitle(title);
		}

		this.modal().show();

		return this;
	};

	this.init();

	return this;
};

/**
 * Prototype instance
 * @type {EspressioModalPlugin}
 */
var _espressioModalPlugin = new EspressioModalPlugin();

EspressioModalAlert.prototype = _espressioModalPlugin;
EspressioModalForm.prototype = _espressioModalPlugin;
EspressioModalConfirm.prototype = _espressioModalPlugin;



